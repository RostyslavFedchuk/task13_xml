<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <style>
                    .tableClass{
                        background-color: lightblue;
                        border: 4px solid black;
                        text-align: center;
                        font-size: 22px;
                        font-family: Times New Roman;
                        padding: 16px;
                    }
                    td{
                        background-color: gray;
                        color: white;
                        text-align: left;
                        width: 250px;
                        border: 2px solid white;
                        margin-right: 10px;
                    }
                    th{
                        width: 250px;
                        border-right: 2px solid black;
                        background-color: lightgreen;
                    }
                </style>
                <title>Candies</title>
            </head>
            <body>
                <table class="tableClass">
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Energy</th>
                        <th>Water</th>
                        <th>Sugar</th>
                        <th>Fructose</th>
                        <th>Chocolate type</th>
                        <th>Vanilin</th>
                        <th>Protein</th>
                        <th>Carbohydrates</th>
                        <th>Fats</th>
                        <th>Production</th>
                    </tr>
                    <xsl:for-each select="candies/candy">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="@type"/></td>
                            <td><xsl:value-of select="energy"/></td>
                            <td><xsl:value-of select="ingredients/water"/></td>
                            <td><xsl:value-of select="ingredients/sugar"/></td>
                            <td><xsl:value-of select="ingredients/fructose"/></td>
                            <td><xsl:value-of select="ingredients/@chocolateType"/></td>
                            <td><xsl:value-of select="ingredients/vanilin"/></td>
                            <td><xsl:value-of select="values/proteins"/></td>
                            <td><xsl:value-of select="values/carbohydrates"/></td>
                            <td><xsl:value-of select="values/fats"/></td>
                            <td><xsl:value-of select="production"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>