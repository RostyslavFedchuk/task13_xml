package com.epam.xmlConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ResourceBundle;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XmlToHtmlConverter {
    private static Logger logger = LogManager.getLogger(XmlToHtmlConverter.class);
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");

    public static void main(String args[]) {
        Source xml = new StreamSource(new File(bundle.getString("CandyXML")));
        Source xslt = new StreamSource(bundle.getString("CandyXSL"));
        convertXMLToHTML(xml, xslt);
    }

    private static void convertXMLToHTML(Source xml, Source xslt) {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter(bundle.getString("CandiesHTML"));
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslt);
            transform.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            logger.info("candies.html generated successfully.");
        } catch (IOException | TransformerConfigurationException e) {
            logger.info("TransformerConfigurationException");
        } catch (TransformerFactoryConfigurationError e) {
            logger.info("TransformerFactoryConfigurationError");
        } catch (TransformerException e) {
            logger.info("TransformerException");
        }
    }

}
