package com.epam.parser.sax;

import com.epam.model.Candy;
import com.epam.parser.xmlValidator.XmlValidator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class SaxParser {

    public static List<Candy> parse(File xml, File xsd) {
        List<Candy> candies = new ArrayList<>();
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
            saxParserFactory.setValidating(true);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxReader saxReader = new SaxReader();
            saxParser.parse(xml, saxReader);
            candies = saxReader.getCandyList();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
        return candies;
    }
}