package com.epam.parser.sax;

import com.epam.model.Candy;
import com.epam.model.Ingredient;
import com.epam.model.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxReader extends DefaultHandler {
    private List<Candy> candyList = new ArrayList<>();
    private Candy candy;
    private Ingredient ingredients;
    private Value values;

    private boolean bName = false;
    private boolean bEnergy = false;
    private boolean bWater = false;
    private boolean bSugar = false;
    private boolean bFructose = false;
    private boolean bVanilin = false;
    private boolean bProteins = false;
    private boolean bCarbohydrates = false;
    private boolean bFats = false;
    private boolean bProduction = false;

    List<Candy> getCandyList() {
        return this.candyList;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candy = new Candy();
            candy.setType(attributes.getValue("type"));
        } else if (qName.equalsIgnoreCase("name")) {
            bName = true;
        } else if (qName.equalsIgnoreCase("energy")) {
            bEnergy = true;
        } else if (qName.equalsIgnoreCase("ingredients")) {
            ingredients = new Ingredient();
            ingredients.setChocolateType(attributes.getValue("chocolateType"));
        } else if (qName.equalsIgnoreCase("water")) {
            bWater = true;
        } else if (qName.equalsIgnoreCase("sugar")) {
            bSugar = true;
        } else if (qName.equalsIgnoreCase("fructose")) {
            bFructose = true;
        } else if (qName.equalsIgnoreCase("vanilin")) {
            bVanilin = true;
        } else if (qName.equalsIgnoreCase("values")) {
            values = new Value();
        } else if (qName.equalsIgnoreCase("proteins")) {
            bProteins = true;
        } else if (qName.equalsIgnoreCase("carbohydrates")) {
            bCarbohydrates = true;
        } else if (qName.equalsIgnoreCase("fats")) {
            bFats = true;
        } else if (qName.equalsIgnoreCase("production")) {
            bProduction = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candyList.add(candy);
            candy = null;
        } else if (qName.equalsIgnoreCase("ingredients")) {
            candy.setIngridients(ingredients);
            ingredients = null;
        } else if (qName.equalsIgnoreCase("values")) {
            candy.setValues(values);
            values = null;
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bName) {
            candy.setName(new String(ch, start, length));
            bName = false;
        } else if (bEnergy) {
            candy.setEnergy(Integer.parseInt(new String(ch, start, length)));
            bEnergy = false;
        } else if (bWater) {
            ingredients.setWater(Double.parseDouble(new String(ch, start, length)));
            bWater = false;
        } else if (bSugar) {
            ingredients.setSugar(Double.parseDouble(new String(ch, start, length)));
            bSugar = false;
        } else if (bFructose) {
            ingredients.setFructose(Double.parseDouble(new String(ch, start, length)));
            bFructose = false;
        } else if (bVanilin) {
            ingredients.setVanilin(Double.parseDouble(new String(ch, start, length)));
            bVanilin = false;
        } else if (bProteins) {
            values.setProteins(Double.parseDouble(new String(ch, start, length)));
            bProteins = false;
        } else if (bCarbohydrates) {
            values.setCarbohydrates(Double.parseDouble(new String(ch, start, length)));
            bCarbohydrates = false;
        } else if (bFats) {
            values.setFats(Double.parseDouble(new String(ch, start, length)));
            bFats = false;
        } else if (bProduction) {
            candy.setProduction(new String(ch, start, length));
            bProduction = false;
        }
    }
}

