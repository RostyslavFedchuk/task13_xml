package com.epam.parser.stax;

import com.epam.model.Candy;
import com.epam.model.Ingredient;
import com.epam.model.Value;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxParser {

    private static XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private static List<Candy> candyList = new ArrayList<>();
    private static Candy candy;
    private static Ingredient ingredients;
    private static Value values;

    public static List<Candy> parse(File xml) {
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "candy": {
                            candy = new Candy();
                            Attribute type = startElement.getAttributeByName(new QName("type"));
                            if (type != null) {
                                candy.setType(type.getValue());
                            }
                            break;
                        }
                        case "name": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setName(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case "energy": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setEnergy(Integer.valueOf(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "ingredients": {
                            ingredients = new Ingredient();
                            Attribute type = startElement.getAttributeByName(new QName("chocolateType"));
                            if (type != null) {
                                ingredients.setChocolateType(type.getValue());
                            }
                            break;
                        }
                        case "water": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setWater(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "sugar": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setSugar(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "fructose": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setFructose(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "vanilin": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.setVanilin(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "values": {
                            values = new Value();
                            break;
                        }
                        case "proteins": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert values != null;
                            values.setProteins(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "carbohydrates": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert values != null;
                            values.setCarbohydrates(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "fats": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert values != null;
                            values.setFats(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case "production": {
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candy != null;
                            candy.setProduction(xmlEvent.asCharacters().getData());
                            break;
                        }
                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    String name = endElement.getName().getLocalPart();
                    switch (name) {
                        case "candy": {
                            candyList.add(candy);
                            candy = null;
                            break;
                        }
                        case "values": {
                            candy.setValues(values);
                            values = null;
                            break;
                        }
                        case "ingredients": {
                            candy.setIngridients(ingredients);
                            ingredients = null;
                            break;
                        }
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return candyList;
    }
}

