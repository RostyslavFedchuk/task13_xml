package com.epam.parser.dom;

import com.epam.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DomReader {
    List<Candy> readDoc(Document doc) {
        List<Candy> candies = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("candy");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Candy candy = new Candy();
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                candy.setName(element.getElementsByTagName("name").item(0).getTextContent());
                candy.setType(element.getAttribute("type"));
                candy.setEnergy(Integer.parseInt(element.getElementsByTagName("energy").item(0).getTextContent()));
                candy.setProduction(element.getElementsByTagName("production").item(0).getTextContent());
                candy.setIngridients(getIngredients(element.getElementsByTagName("ingredients")));
                candy.setValues(getValues(element.getElementsByTagName("values")));
                candies.add(candy);
            }
        }
        return candies;
    }

    private Ingredient getIngredients(NodeList nodes) {
        Ingredient ingredients = new Ingredient();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            ingredients.setAll(Double.valueOf(element.getElementsByTagName("water").item(0).getTextContent()),
                    Double.valueOf(element.getElementsByTagName("sugar").item(0).getTextContent()),
                    Double.valueOf(element.getElementsByTagName("fructose").item(0).getTextContent()),
                    Double.valueOf(element.getElementsByTagName("vanilin").item(0).getTextContent()),
                    element.getAttribute("chocolateType"));
        }
        return ingredients;
    }

    private Value getValues(NodeList nodes) {
        Value values = new Value();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            values.setAll(Double.parseDouble(element.getElementsByTagName("proteins").item(0).getTextContent()),
                    Double.parseDouble(element.getElementsByTagName("carbohydrates").item(0).getTextContent()),
                    Double.parseDouble(element.getElementsByTagName("fats").item(0).getTextContent()));
        }
        return values;
    }
}
