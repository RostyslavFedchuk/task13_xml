package com.epam.parser;

import com.epam.model.Candy;
import com.epam.parser.dom.DomParser;
import com.epam.parser.sax.SaxParser;
import com.epam.parser.stax.StaxParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class Parser {
    private static Logger logger = LogManager.getLogger(Parser.class);

    public static void main(String[] args) {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        File xml = new File(bundle.getString("CandyXML"));
        File xsd = new File(bundle.getString("CandyXSD"));

        printList(SaxParser.parse(xml, xsd), "SAX");
        printList(StaxParser.parse(xml), "StAX");
        printList(DomParser.getCandyList(xml, xsd), "DOM");
    }

    private static void printList(List<Candy> candies, String parserName) {
        candies.sort(Comparator.comparing(Candy::getEnergy));
        logger.info(parserName + "\n");
        for (int i = 0; i < candies.size(); i++) {
            logger.info((i + 1) + "." + candies.get(i) + "\n");
        }
    }
}
