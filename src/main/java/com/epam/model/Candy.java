package com.epam.model;

public class Candy {
    private String name;
    private int energy;
    private String type;
    private Ingredient ingredients;
    private Value value;
    private String production;

    public Candy(){}

    public Candy(String name, int energy, String type, Ingredient ingredients,
                 Value value, String production) {
        this.name = name;
        this.energy = energy;
        this.type = type;
        this.ingredients = ingredients;
        this.value = value;
        this.production = production;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Ingredient getIngredients() {
        return ingredients;
    }

    public void setIngridients(Ingredient ingredients) {
        this.ingredients = ingredients;
    }

    public Value getValues() {
        return value;
    }

    public void setValues(Value value) {
        this.value = value;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    @Override
    public String toString() {
        return "Candy{ name=" + name + ", Energy=" + energy + "kcal, Type="
                + type + "\n\tIngredient" + ingredients + "\tValues"
                + value + ", Production=" + production + "}";
    }
}
